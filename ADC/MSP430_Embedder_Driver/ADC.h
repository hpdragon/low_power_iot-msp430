#ifndef ADC_H_
#define ADC_H_

#include <msp430g2553.h>

#include <stdint.h>

//MODE
//this mode require interrupt
#define MODE_LOW_POWER

void ADC_Config(void);

uint16_t ADC_GetValue(void);

#endif 
