#include <msp430.h> 

#include "uart.h"
#include "MSP430_Embedder_Driver/ADC.h"
#include "MSP430_Embedder_Driver/interrupt.h"

#include <stdio.h>
#include <stdlib.h>

#define MODE_DEBUG_UART
#define MODE_TestCircuit

/*
 * main.c
 */
int main(void) {
    WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer

    DCOCTL = 0;
	BCSCTL1 = CALBC1_1MHZ;
    DCOCTL = CALDCO_1MHZ;

#ifdef MODE_DEBUG_UART
    Configure_UART();


#endif

#ifdef MODE_TestCircuit
    // Set P1.4 to output direction
    P2DIR |= BIT0;
    P2OUT &= ~(BIT0);
#endif

    uint16_t data = 0 ;
    char *string;
    string = (char*)calloc(10,sizeof(unsigned char));

    ADC_Config();

#ifdef MODE_DEBUG_UART
    PrintStr("ADC setup completed\n");
#endif

    while(1)
    {
    	data = ADC_GetValue();
    	sprintf(string,"%d",data);
#ifdef MODE_DEBUG_UART
    	PrintStr(string);
    	PrintStr("\n");
    	_delay_cycles(20000);
#endif
    }
}
