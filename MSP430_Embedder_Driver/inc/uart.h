/*
 * uart.h
 *
 *  Created on: Mar 10, 2017
 *      Author: Bao
 */

#ifndef UART_H_
#define UART_H_

/*Prototype
 */
void Configure_UART(void);
void UartSendByte(unsigned char byte);
void PrintStr(char *pData);
#endif /* UART_H_ */
