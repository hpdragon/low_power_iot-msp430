/*
 * MyDS18B20.h
 *
 *  Created on: Mar 15, 2017
 *      Author: Phong
 */

#ifndef DS18B20_H_
#define DS18B20_H_

#include <msp430g2553.h>
#include "config.h"

#ifdef MODE_LOW_POWER
#define _ds_delay(x)        LMDELAY_Delay_us(x)
#else
#define _ds_delay(x)        _delay_cycles(x)
#endif

//onewire port, pin

#define OWDIR 	P1DIR
#define OWOUT 	P1OUT
#define OWIN	P1IN
#define OWREN	P1REN
#define OWPIN	BIT3

//function
#define OW_LO	{OWDIR|=OWPIN; OWREN &= ~OWPIN; OWOUT &= ~OWPIN;}
#define OW_HI	{OWDIR|=OWPIN; OWREN &= ~OWPIN; OWOUT |= OWPIN;}
//OW release: read slave stt
#define OW_RLS	{OWDIR &=~OWPIN; OWREN |=OWPIN; OWOUT |= OWPIN;/*Internal resistor pull up*/}

//DS18B20 address
#define DS1820_CONVERT_T            0x44

#define DS1820_SKIP_ROM             0xCC

#define DS1820_READ_SCRATCHPAD      0xBE
#define DS1820_WRITE_SCRATCHPAD		0x4E
#define DS1820_COPY_SCRATCHPAD		0x48

#define DS1820_RECALL_E2			0xB8

#define DS1820_Read_Power_Supply	0xB4

//for DS18B20
void DS_Init(void);

uint16_t DS_ReadDS18B20(void);

uint16_t DS_GetData(void);

//Prototye

static uint8_t onewire_reset(void);

static void onewire_writebit(uint8_t);

static uint8_t onewire_readbit(void);

static void onewire_write_byte(uint8_t);

static uint8_t onewire_read_byte(void);

#endif /* DS18B20_H_ */
