#include "lm_delay.h"

#include <msp430.h>
#include <stdint.h>

/*
 * init delay using low power mode
 * base on sub main clock
 * using timer 1
 */
void LMDelay_Init(void)
{
    // CCR0 interrupt enabled
    CCTL0 = CCIE;

    // SMCLK, upmode
    TACTL = TASSEL_2 + MC_1;
}

/*
 * delay using low power mode
 * us
 */
void LMDELAY_Delay_us(uint16_t delay)
{
    //delay time
    TACCR0 = delay;

    LMDEALY_TIMER0_Start

    // Enter LPM3 + enable interrupt
    __bis_SR_register(LPM1_bits + GIE);
}

void LMDELAY_Delay_longus(uint32_t delay)
{
	while (delay > 65535)
	{
		LMDELAY_Delay_us(65535);
		delay -= 65535;
	}
	LMDELAY_Delay_us(delay);
}
