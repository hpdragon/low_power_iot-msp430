/*
 * uart.c
 *
 *  Created on: Mar 10, 2017
 *      Author: Bao
 */

#include "uart.h"
#include <MSP430.h>
void Configure_UART(void)
{
	P1SEL = BIT1 + BIT2;
	P1SEL2 = BIT1 + BIT2;
	UCA0CTL1 |= UCSWRST; /* USCI Software Reset */

	UCA0CTL0 = 0x00; //No parity bit, 8bit, 1 stop bit...

	UCA0CTL1 = UCSSEL_2 | UCSWRST; //* USCI 0 Clock Source: SMCLK */
	/*Configure Baud rate*/
	UCA0MCTL = UCBRF_8 | UCBRS_0 |UCOS16;
	UCA0BR0 = 6; // 1MHz . Baud rate = 9600. Following table 15-5
	UCA0BR1 = 00;


	UCA0CTL1 &= ~UCSWRST; // Reset module
}

void UartSendByte(unsigned char byte)
{
	while (!(IFG2 & UCA0TXIFG)); // Wait for the transmit buffer to be ready
								//Neu khong co se bi trung lap du lieu (nghe noi vay)

	 UCA0TXBUF = byte; // Transmit Buffer
}
void PrintStr(char *pData)
{
	unsigned char i = 0;
	while(pData[i])
	{
		while (!(IFG2 & UCA0TXIFG));
		UartSendByte(pData[i]);
		i++;
	}
}
