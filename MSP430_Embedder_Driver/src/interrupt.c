#include "interrupt.h"

#include "lm_delay.h"

//timer0 interrupt
#pragma vector = TIMER0_A0_VECTOR
__interrupt void myTimerISR(void)
{
	//Stop Timer_A
	LMDELAY_TIMER0_Stop

	//Clear LPM0 bits from 0(SR)
    __bic_SR_register_on_exit(LPM1_bits);
}

// ADC10 interrupt service routine
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=ADC10_VECTOR
__interrupt void ADC10_ISR(void)
#elif defined(__GNUC__)
void __attribute__ ((interrupt(ADC10_VECTOR))) ADC10_ISR (void)
#else
#error Compiler not supported!
#endif
{
	ADC10CTL0 &= ~(ADC10ON + ENC);
  __bic_SR_register_on_exit(CPUOFF);        // Clear CPUOFF bit from 0(SR)
}
