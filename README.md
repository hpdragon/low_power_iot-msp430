# Low Power IoT - MSP430

A low power IoT device that measures earth temperature and humility and send these data to a computer for further processing. This device can last for months with 1 AAA battery.
