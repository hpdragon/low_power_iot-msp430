LABCENTER PROTEUS TOOL INFORMATION FILE
=======================================

In case of difficulty, please e-mail support@labcenter.co.uk

Tool set up for Proteus layout 'node.pdsprj'.
CADCAM generated at 6:54:25 PM on Wednesday, April 26, 2017.

File List
---------
Bottom Copper           : node - CADCAM Bottom Copper.GBR
Drill, Plated, 1-16     : node - CADCAM Drill L1 L16 Plated.GBR
Netlist                 : node - CADCAM Netlist.IPC

Photoplotter Setup
------------------
Format: X2, ASCII, 4.3, metric, absolute, eob=*, LZO
Notes:  D=Diameter, S=Side, W=Width, H=Height, C=Chamfer, I=Index

D10	CIRCLE  D=0.3048mm                                                        DRAW  Conductor
D11	CIRCLE  D=0.381mm                                                         DRAW  Conductor
D12	CIRCLE  D=0.254mm                                                         DRAW  Conductor
D13	PPAD    W=2.54mm              H=1.27mm              I=0                   FLASH ComponentPad
D14	PPAD    W=1.27mm              H=2.54mm              I=1                   FLASH ComponentPad
D15	SQUARE  S=1.905mm                                                         FLASH ComponentPad
D16	SQUARE  S=2.032mm                                                         FLASH ComponentPad
D17	RECT    W=0.889mm             H=1.016mm                                   FLASH SMDPad,CuDef
D18	RECT    W=1.27mm              H=1.016mm                                   FLASH SMDPad,CuDef
D19	RECT    W=1.016mm             H=1.27mm                                    FLASH SMDPad,CuDef
D70	CIRCLE  D=0.2032mm                                                        DRAW  Profile
D71	CIRCLE  D=0.508mm                                                         FLASH ComponentDrill
D20	CIRCLE  D=0.762mm                                                         FLASH ComponentDrill
D21	CIRCLE  D=1.016mm                                                         FLASH ComponentDrill


[END OF FILE]
