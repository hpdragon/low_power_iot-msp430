#include <msp430.h> 
#include "uart.h"

#include "DS18B20.h"
#include "ADC.h"
#include "lm_delay.h"
#include "interrupt.h"

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#define MODE_DEBUG_UART
//#define MODE_TestCircuit

/*
 * main.c
 */
int main(void)
{
	WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer

    DCOCTL = 0;
	BCSCTL1 = CALBC1_1MHZ;
    DCOCTL = CALDCO_1MHZ;

	LMDelay_Init();

#ifdef MODE_DEBUG_UART
    Configure_UART();
#endif

#ifdef MODE_TestCircuit
    // Set P1.4 to output direction
    P2DIR |= BIT2;
    P2OUT &= ~(BIT2);
#endif

    uint16_t data = 0 ;
    char string[5];
    //string = (char*)calloc(5,sizeof(unsigned char));

    DS_Init();

#ifdef MODE_DEBUG_UART
    PrintStr("DS18B20 setup completed\n");
#endif

    while (1)
    {
    	data = DS_GetData();
    	sprintf(string,"%d",data);

#ifdef MODE_DEBUG_UART
    	PrintStr(string);
    	PrintStr("\n");
#endif
    }
}
