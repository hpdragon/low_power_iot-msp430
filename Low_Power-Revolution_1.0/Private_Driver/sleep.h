#ifndef SLEEP_H_
#define SLEEP_H_

#include <msp430g2553.h>
#include "config.h"

//define for timer
#define SLEEP_Start                    TACTL |= (MC_1);
#define SLEEP_Stop                     TACTL &=~(MC_1);
#define SLEEP_SleepTime                15000

void SLEEP_Init(void);

#endif /* SLEEP_H_ */
