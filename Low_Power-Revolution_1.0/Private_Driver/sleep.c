#include "sleep.h"

void SLEEP_Init(void)
{
    // CCR0 interrupt enabled
    CCTL0 = CCIE;
    //delay time
    TACCR0 = SLEEP_SleepTime;
    // ACLK, upmode
    TACTL = TASSEL_1 + MC_1;
}
