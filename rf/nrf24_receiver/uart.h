
#ifndef UART_H_
#define UART_H_

/*Prototype
 */
void uart_init(void);
void uart_send_char(char character);
void uart_send_str(char *string, short int str_lenght);
#endif /* UART_H_ */
