
#ifndef RF24_H_
#define RF24_H_


void IO_RF24L01_initial(void);
void init_NRF24L01(void);
unsigned char nRF24L01_SPI_RW(unsigned char Buff);
unsigned char nRF24L01_SPI_Read(unsigned char reg);
unsigned char nRF24L01_SPI_RW_Reg(unsigned char reg, unsigned char value);
unsigned char nRF24L01_SPI_Read_Buf(unsigned char reg, unsigned char *pBuf, unsigned char uchars);
unsigned char nRF24L01_SPI_Write_Buf(unsigned char reg, unsigned char *pBuf, unsigned uchars);
void nRF24L01_SetRX_Mode(void);
void nRF24L01_SetTX_Mode(void);
void nRF24L01_SetChannel(unsigned int channel);
void nRF24L01_ClearSend(void);
unsigned char nRF24L01_RxPacket(unsigned char* rx_buf);
void nRF24L01_TxPacket(unsigned char* tx_buf);

#endif
