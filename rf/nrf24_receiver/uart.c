#include "uart.h"
#include <MSP430.h>

#define RXPIN	BIT1
#define TXPIN	BIT2

#define UPORTSEL	P1SEL
#define UPORTSEL2	P1SEL2

void uart_init(void)
{
	//config tx, rx port
	UPORTSEL |= TXPIN;
	UPORTSEL2 |= TXPIN;

	UCA0CTL1 |= UCSWRST;

	UCA0CTL0 |= 0x00; //No parity, LSB first, 8 bit, 1 stop bit
	UCA0CTL1 |= UCSSEL_2 ; //UART clk: SMCLK => low power mode

	//UCBRx = 6 = 6 + 0*256
	UCA0BR0 = 0x06;
	UCA0BR1 = 0x00;

	//Config baudrate: 9600 || Baudrate clock: 1MHz || UCBRS0 || UCBRF8
	UCA0MCTL |= (UCBRS_0 + UCBRF_8 + UCOS16);

	UCA0CTL1 &= ~UCSWRST; // Reset module
}

void uart_send_char(char character)
{
	while (!(IFG2 & UCA0TXIFG))
		{;}
	UCA0TXBUF = character;
}

void uart_send_str(char *string, short int str_lenght)
{
	while(str_lenght--)
	{
		uart_send_char(*string);
		*string++;
	}
}
