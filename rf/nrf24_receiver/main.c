#include <msp430.h>
#include <stdlib.h>
#include "RF24.h"
#include "delay.h"
#include "uart.h"
char RxBuffer[2];
void main()
{
	WDTCTL  = WDTPW + WDTHOLD;
	BCSCTL1 = CALBC1_1MHZ;
	DCOCTL  = CALDCO_1MHZ;


	uart_init();

	init_NRF24L01();
	nRF24L01_SetRX_Mode();
	uart_send_str("\n Hello!!!",11);


  while(1)
  {

	P1OUT |= BIT7; //CE
	delay_us(10);
	if((P2IN & BIT4)==0)
    {
		nRF24L01_RxPacket(RxBuffer); // Rx buffer data
		nRF24L01_ClearSend();     // Clear Bit Send
		uart_send_str("\n",2);
		uart_send_str(RxBuffer,2);
		delay_ms(22);
		delay_us(4);
		P1OUT &= ~BIT7;
    }
  }
}
