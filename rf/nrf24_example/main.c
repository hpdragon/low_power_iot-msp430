/*************************************************************************
DESCRIPTION - Phat
Project su dung Rf24L01 de truyen phat
Author: Huu Nho 
*************************************************************************/
#include <msp430.h>
#include "RF24.h"
#include "delay.h"
#include "string.h"
volatile int ta0 = 0;
volatile unsigned int i = 0;

unsigned char TxBuffer[5][2];
void main()
{
	unsigned char temperature[5] = "98765";
	unsigned char humid[5] = "01234";
	WDTCTL = WDTPW + WDTHOLD;     //Stop WatchDog Timer
	BCSCTL1 = CALBC1_1MHZ;            // Set DCO to 1 MHz
	DCOCTL = CALDCO_1MHZ;

	//=============Timer Initial=================
	//setup for timer0 interrupt
	TA0CCTL0 = CCIE;                           // CCR0 interrupt enabled
	TA0CCR0 = 5000;
	TA0CTL = TASSEL_2 + MC_1;
	//===========================================



	init_NRF24L01();
	nRF24L01_SetTX_Mode();

  for(i = 0; i < 5; i++)
  {
	  TxBuffer[i][0] = temperature[i];
	  TxBuffer[i][1] = humid[i];
  }
  i = 0;
  __bis_SR_register(GIE);           //Enable interrupt
  while(1)
  {   

  }
}

#pragma vector=TIMER0_A0_VECTOR
__interrupt void Timer0_A0(void)
{
  ta0++;
  if(ta0==200)
  {
    nRF24L01_ClearSend();     // Clear Bit Send
    nRF24L01_TxPacket(TxBuffer[i]); // Transmit Tx buffer data
    ta0=0;

     if( 4 == i)
    	 i = 0;
     else
    	 i++;
     delay_ms(2);
  }
}

 
