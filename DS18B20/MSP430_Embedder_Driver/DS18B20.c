/*1-Wire DS18B20 for MSP430
 * Author : Phong
 * References : David Siroky's code
 * 				ytuongnhanh.vn
 * 				Datasheeet DS18B20
 * 	Created on: Mar 19,2017
 */
#include "DS18B20.h"

#include <msp430.h>
#include <stdint.h>
#include "lm_delay.h"

/*Using Clk =1MHz*/
/*The delay cycles are unstable, test and change until program completely work*/

//-----------------------------------------------------------------

/*
 * Setup gpio for onewire
 */
void DS_Init(void)
{
	OWDIR |= OWPIN;
	OWOUT |= OWPIN;
	OWREN |= OWPIN;
}

/*
 * For DS18B20 only
*/
uint16_t DS_ReadDS18B20(void)
{
	uint8_t i;
	uint16_t byte = 0;
	for(i=0; i < 16 ; i++)
	{
		byte >>=1;
		if (onewire_readbit())
			byte |=0x8000;
	}
	return byte;
}

/*
 * get data from ds18b20
 */
uint16_t DS_GetData(void)
{
	uint16_t temp = 0;
	onewire_reset();
	//write 0xcc-Skip ROM, address all devices
	onewire_write_byte(DS1820_SKIP_ROM);
	//write 0x44-Start temperature conversion
	onewire_write_byte(DS1820_CONVERT_T);
	OW_HI
	//delay 750ms for default 12-bit resolution
	//according to datasheet DS18B20, page 3
	LMDELAY_Delay_longus(750000);

	onewire_reset();
	//write 0xcc
	onewire_write_byte(DS1820_SKIP_ROM);
	//write 0xbe-Read scratchpad(temp register)
	onewire_write_byte(DS1820_READ_SCRATCHPAD);
	temp = DS_ReadDS18B20();

#ifndef MODE_REDUCE_COMPUTATIONAL
	if(temp<0x8000)
	{
		return(temp*0.0625);
	}
	else
	{
		temp=(~temp)+1;
		return(temp*0.0625);
	}
#endif

	return temp;
}

//-----------------------------------------------------------------

/*
 * For 1wire sensors
 * 0 : Succeeded
 * 1 : Failed - line wasn't pulled down
 * 2 : Failed - Slave didn't send presence signal
 */
static uint8_t onewire_reset(void)
{
	OW_LO
	_ds_delay(550);	//480us minimum = 8 time slot
	OW_RLS				//Release line for resetting
	_ds_delay(60); 	// Slave waits 15-60us
	if (OWIN & OWPIN)	// line should be pulled down by slave
		return 1;
	_ds_delay(240); // Presence pulse signal from slave 60-240us
	if (!(OWIN & OWPIN))// line should be "released" by slave
		return 2;
	return 0;
}

/*
 * write one bit to onewire
 */
static void onewire_writebit(uint8_t bit)
{
	_delay_cycles(2); // recovery
	OW_LO
	if(bit) //write 1
		_delay_cycles(6); //line was pulled down by master 1-15us
	else	//write 0
		_ds_delay(64); //line was pulled up by master 60-120us
	/*Release line in rest of write slot*/
	OW_RLS
	if (bit) //1
		_ds_delay(64);
	else 	//0
		_delay_cycles(6);
}

/*
 * read one bit from onewire
 */
static uint8_t onewire_readbit(void)
{
	uint8_t bit =0;
	_delay_cycles(2); //recovery
	OW_LO
	_delay_cycles(5); //line was pulled down by master 0-15us
	OW_RLS
	_delay_cycles(6); //15us window - 15us sampling (according to the theory)
	if (OWIN & OWPIN)
		bit = 1; //read bit
	_ds_delay(60);// rest of the read slot
	return bit;
}

/*
 * write one byte to onewire
 */
static void onewire_write_byte(uint8_t byte)
{
	uint8_t i;
	for( i = 0; i < 8; i++)
	{
		onewire_writebit(byte & 1);
		byte >>=1;
	}
}

/*
 * read one byte from onewire
 */
static uint8_t onewire_read_byte(void)
{
	uint8_t i;
	uint8_t byte = 0;
	for(i = 0; i < 8; i++)
	{
		byte >>=1;
		if (onewire_readbit())
			byte |= 0x80;
	}
	return byte;
}
