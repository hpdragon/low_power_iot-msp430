#ifndef LM_DELAY_H_
#define LM_DELAY_H_

//timer command
#define LMDEALY_TIMER0_Start                    TACTL |= (MC_1);
#define LMDELAY_TIMER0_Stop                     TACTL &=~(MC_1);

/*Prototye------------------------------*/

void LMDelay_Init(void);

void LMDELAY_Delay_us(uint16_t);

void LMDELAY_Delay_longus(uint32_t);

#endif /* LM_DELAY_H_ */
